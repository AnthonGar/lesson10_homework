#include"Customer.h"
#include <iostream>
#include<map>

using namespace std;

int mainMenu();
int itemsMenu(Item itemsList[]);
int optionTwoMenu();
string sign(map<string, Customer> abcCustomers, bool search);

map<string, Customer> registerAndBuy(map<string, Customer> abcCustomers, Item itemList[]);
map<string, Customer> signAndBuy(map<string, Customer> abcCustomers, Item itemList[]);
map<string, Customer> whoPaysTheMost(map<string, Customer> abcCustomers, Item itemList[]);

int main()
{
	bool run = true;
	int option = 0;
	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	while (run)
	{
		option = mainMenu();
		switch (option)
		{
		case 1:
			abcCustomers = registerAndBuy(abcCustomers, itemList);
			break;

		case 2:
			abcCustomers = signAndBuy(abcCustomers, itemList);
			break;

		case 3:
			abcCustomers = whoPaysTheMost(abcCustomers, itemList);
			break;

		case 4:
			run = false;
			break;

		default:
			cout << "Pleace pick between 1 - 4." << endl;
			break;
		}
	}


	return 0;
}

//This func will print the main menu and recive the users input.
int mainMenu()
{
	int userInput;
	system("cls");
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.    to sign as customer and buy items" << endl;
	cout << "2.    to uptade existing customer's items" << endl;
	cout << "3.    to print the customer who pays the most" << endl;
	cout << "4.    to exit" << endl;
	cin >> userInput;
	cin.clear();
	return userInput;
}

//This func will print all the items and recive users input.
int itemsMenu(Item itemsList[])
{
	int i, userInput;

	//printing the items
	cout << "The items you can buy are: (0 to exit)" << endl;
	for (i = 0; i < 10; i++)
	{
		cout << (i + 1) << ".    " << itemsList[i].getName() << " price: " << itemsList[i].getUnitPrice() << endl;
	}

	//validating the input
	do
	{
		cout << "What item would you like to buy? Input:";
		cin >> userInput;
		cin.clear();
	} while (!(userInput >= 0 && userInput <= 10));

	system("cls");

	return userInput;
}

int optionTwoMenu()
{
	int userInput;

	do
	{
		//Printing the menu.
		cout << "1.    Add items" << endl;
		cout << "2.    Remove items" << endl;
		cout << "3.    Back to menu" << endl;
		cout << "What would you like to do ? Input : ";

		cin >> userInput;
		cin.clear();
		system("cls");
		//Checking the input.
	} while (!(userInput >= 1 && userInput <= 3));

	return userInput;
}

//This func will take care of the sign-in process.
string sign(map<string, Customer> abcCustomers, bool search)
{
	bool valid = false;
	string userInput;

	map<string, Customer>::iterator it;

	while (!valid)
	{
		//User input
		cout << "Customer's name: ";
		cin >> userInput;
		cin.clear();

		it = abcCustomers.find(userInput);

		//NOT searching for a name (checking if exists).
		if (!search)
		{
			//If the name wasnt found
			if (it == abcCustomers.end())
				valid = true;
			//if the name was found
			else
				cout << "Name was found, try again" << endl;
		}
		//Searching for a name.
		else //if (search)
		{
			//If the name wasnt found
			if (it == abcCustomers.end())
				cout << "Name wasn't found, try again" << endl;
			//if the name was found
			else
				valid = true;
		}
	}

	system("cls");

	return userInput;
}

//This function will register new customer
map<string, Customer> registerAndBuy(map<string, Customer> abcCustomers, Item itemList[])
{
	int item;
	string customer = sign(abcCustomers, false);
	Customer c = Customer(customer);

	//Adding items to the customer's shopping list.
	do
	{
		item = itemsMenu(itemList);
		if (item != 0)//Item to add (the func will take care of the count if needed).
			c.addItem(itemList[item - 1]);
	} while (item != 0);

	//Adding the new customer to the map.
	abcCustomers[customer] = c;

	return abcCustomers;
}

map<string, Customer> signAndBuy(map<string, Customer> abcCustomers, Item itemList[])
{
	bool run = true;
	int item;
	int option;
	string customer = sign(abcCustomers, true);

	while (run)
	{
		//printing the customer's items.
		abcCustomers[customer].print();

		//printing the user's options.
		option = optionTwoMenu();
		switch (option)
		{
		case 1:
			//Adding items to the customer's shopping list.
			do
			{
				item = itemsMenu(itemList);
				if (item != 0)//Item to add (the func will take care of the count if needed).
					abcCustomers[customer].addItem(itemList[item - 1]);
			} while (item != 0);
			break;
		case 2:
			//Removing items from the customer's shopping list.
			do
			{
				item = itemsMenu(itemList);
				if (item != 0)//Item to remove (the func will take care of the count if needed).
					abcCustomers[customer].removeItem(itemList[item - 1]);
			} while (item != 0);
			break;
		case 3:
			run = false;
			break;
		default:
			cout << "Pick between 1 - 3" << endl;
			break;
		}
		system("cls");
	}

	return abcCustomers;
}

map<string, Customer> whoPaysTheMost(map<string, Customer> abcCustomers, Item itemList[])
{
	double largestSum = 0;
	Customer bawler;
	map<string, Customer>::iterator it;

	for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
	{
		if (it->second.totalSum() > largestSum)
		{
			largestSum = it->second.totalSum();
			bawler = it->second;
		}
	}
	//If there is something to print.
	if (it != abcCustomers.end())
		cout << "Bawler of the year: " << bawler.getName() << " and his total spendings are:" << largestSum << endl;

	system("pause");

	return abcCustomers;
}