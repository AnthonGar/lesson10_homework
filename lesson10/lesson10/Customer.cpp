#include "Customer.h"

using namespace std;

//C-tor for the customer class.
Customer::Customer(string name) : _name(name)
{

}

//C-tor for the customer class.
Customer::Customer()
{

}

//D-tor for the customer class.
Customer::~Customer()
{

}

//********** SETTERS **********
void Customer::setName(string name)
{
	_name = name;
}

//********** GETTERS **********
string Customer::getName()
{
	return _name;
}

set<Item> Customer::getItems()
{
	return _items;
}

//returns the total sum for payment
double Customer::totalSum() const
{
	double totalSum = 0;
	for (set<Item>::iterator it = _items.begin(); it != _items.end(); ++it)
		totalSum += it->totalPrice();
	return totalSum;
}

//add item to the set
void Customer::addItem(Item item)
{
	//Trying to locate the item
	set<Item>::iterator it = _items.find(item);
	//If the item was found --> inc the count of the item.
	if (it != _items.end())
	{
		//Inc the count of the item.
		Item inc = *it;
		inc.setCount(inc.getCount() + 1);

		//Changing the set.
		_items.erase(*it);
		_items.insert(inc);
	}
	//if the item wasnt found --> add the item
	else
	{
		_items.insert(item);
	}
}

//remove item from the set
void Customer::removeItem(Item item)
{
	//Trying to locate the item
	set<Item>::iterator it = _items.find(item);
	//If the item was found --> remove it.
	if (it != _items.end())
	{
		//If there are multple items of the same kind.
		if (item.getCount() != 1)
		{
			//Dec the count of the item.
			Item dec = *it;
			dec.setCount(dec.getCount() - 1);

			//Changing the set.
			_items.erase(*it);
			_items.insert(dec);
		}
		//If there is a single item of the same kind.
		else
		{
			_items.insert(item);
		}
	}
	//If the item wasnt found --> error msg.
	{
		cout << "No such Item exists." << endl;
	}
}

void Customer::print()
{
	cout << getName() << "'s items:" << endl;
	for (set<Item>::iterator it = _items.begin(); it != _items.end(); ++it)
		cout << it->getSerialNumber() << ": " << it->getName() << " - " << it->getUnitPrice() << " * " << it->getCount() << "    total for the item: " << it->totalPrice() << endl;
	cout << "_____________________________________" << endl;
	cout << "Total Payment Required: " << totalSum() << endl;
}