#include "Item.h"

using namespace std;

//C-tor for the Item class.
Item::Item(string name, string serialNumber, double unitPrice) : _name(name), _serialNumber(serialNumber), _count(1), _unitPrice(unitPrice)
{

}

//C-tor for the Item class.
Item::Item()
{

}

//D-tor for the Item class.
Item::~Item()
{

}

//This func will return the total price that a customer has to pay for a certain item.
double Item::totalPrice() const
{
	const int count = Item::getCount();
	const double price = Item::getUnitPrice();
	return (count * price);
}

//********** OPERATORS **********
bool Item::operator <(const Item& other) const
{
	return ((this->_serialNumber) < (other._serialNumber));
}

bool Item::operator >(const Item& other) const
{
	return ((this->_serialNumber) > (other._serialNumber));
}

bool Item::operator ==(const Item& other) const
{
	return ((this->_serialNumber) == (other._serialNumber));
}

//********** GETTERS **********
string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

//********** SETTERS **********
void Item::setName(string name)
{
	_name = name;
}

void Item::setSerialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setCount(const int count)
{
	_count = count;
}

void Item::setUnitPrice(double unitPrice)
{
	_unitPrice = unitPrice;
}